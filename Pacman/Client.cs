﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace Snake
{
    enum Difficulty
    {
        Easy,
        Moderate,
        Hard,
        Insane
    }
    class Client
    {
        public static bool paused = false;
        public static bool isCheating = false;

        public static Color themeColor = Color.Red;
        public static Point gridSize = new Point(50, 25);
        public static Vector2 cellSize;
        public static Difficulty difficulty;

        public static Player player;
        public static List<Pickup> pickups;
        public static float score;
        public static Rectangle Viewport = new Rectangle(0, 0, Gl.graphics.Viewport.Width, Gl.graphics.Viewport.Height - 64);
        public static string message;
        public static float messageAlpha;
        public static Color messageColor;

        public static void LoadContent()
        {
            //Load Content
            Player.LoadContent();
            Pickup.spriteSheet = Gl.Load("PacmanPickups");
        }

        public static void Initialize(Difficulty difficultyLevel)
        {
            //Initialize
            paused = false;
            isCheating = false;
            difficulty = difficultyLevel;
            cellSize.X = (float)Viewport.Width / gridSize.X;
            cellSize.Y = (float)Viewport.Height / gridSize.Y;

            #region Difficulty Levels
            if (difficulty == Difficulty.Easy)
            {
                themeColor = Color.White;
                player = new Player(new Point(0, 0), Direction.Right, 1)
                {
                    loop = false,
                    frameDelayTime = 5,
                    scoreToGetLooping = 50
                };
            }
            else if (difficulty == Difficulty.Moderate)
            {
                themeColor = Color.Yellow;
                player = new Player(new Point(0, 0), Direction.Right, 1)
                {
                    loop = false,
                    frameDelayTime = 3,
                    scoreToGetLooping = 50
                };
            }
            else if (difficulty == Difficulty.Hard)
            {
                themeColor = Color.Red;
                player = new Player(new Point(0, 0), Direction.Right, 1)
                {
                    loop = false,
                    frameDelayTime = 2,
                    scoreToGetLooping = 50
                };
            }
            else
            {
                themeColor = Color.OrangeRed;
                player = new Player(new Point(0, 0), Direction.Right, 1)
                {
                    loop = false,
                    frameDelayTime = 1,
                    scoreToGetLooping = 5
                };
            }
            #endregion

            pickups = new List<Pickup>();
            AddPickup();
            score = 0;
        }
        public static void Reset()
        {
            Initialize(difficulty);
        }
        public static void OpenMenu(bool showScore)
        {
            Gl.game.GotoMenu(MenuState.InGame);
            MenuSystem.showScore = showScore;
            if (showScore)
                MenuSystem.score = score;
        }
        public static void TurnOnCheating()
        {
            isCheating = true;
            Console.Write("Cheating is turned on.", CInType.Warning);
            Console.Write("No high scores will be set in this round.", CInType.Warning);
        }

        public static void Update()
        {
            if (!paused)
            {
                //Update
                player.Update();

                #region Shortcut Keys
                if (Gl.KeyDown(Keys.LeftControl) || Gl.KeyDown(Keys.RightControl))
                {
                }
                else
                {
                    if (Gl.KeyPress(Keys.R))
                    {
                        Reset();
                    }
                    if (Gl.KeyPress(Keys.E))
                    {
                        player.AddTailLength(1);
                    }
                }
                #endregion
                #region Delete Dead Things
                for (int i = 0; i < pickups.Count; i++)
                {
                    if (!pickups[i].alive)
                    {
                        pickups.RemoveAt(i);
                        i--;
                    }
                }
                #endregion
                #region Message
                if (messageAlpha > 0)
                    messageAlpha -= 0.01f;
                #endregion
                if (!TextBox.ActiveBox)
                {
                    View.BasicMovement(5f, true, true);
                }
            }
            #region Shortcut Keys
            if (Gl.KeyDown(Keys.LeftControl) || Gl.KeyDown(Keys.RightControl))
            {
            }
            else
            {
                if (Gl.KeyPress(Keys.P) && !Console.open)
                {
                    paused = !paused;
                }
                if (Gl.KeyPress(Keys.Escape) || Gl.KeyPress(Keys.Back) || Gl.KeyPress(Keys.Tab))
                {
                    if (!paused)
                        OpenMenu(false);
                    else
                        Gl.game.inMenu = false;
                }
            }
            #endregion
        }
        public static void AddPickup()
        {
            bool go = true;
            Point p = Point.Zero;
            while (go)
            {
                go = false;
                p = new Point(Gl.Random(0, gridSize.X - 1), Gl.Random(0, gridSize.Y - 1));
                for (int i = 0; i < player.tail.Count; i++)
                {
                    if (player.tail[i] == p)
                    {
                        go = true;
                        break;
                    }
                }
            }
            pickups.Add(new Pickup(p, new Point((int)(MathHelper.Lerp(0, 1, (float)Gl.rand.NextDouble()) + 0.5f), (int)(MathHelper.Lerp(0, 1, (float)Gl.rand.NextDouble()) + 0.5f))));
        }
        public static void ShowMessage(string messageText, Color color)
        {
            messageAlpha = 1.0f;
            message = messageText;
            messageColor = color;
        }

        public static void Draw()
        {
            Gl.spriteBatch.Begin(SpriteBlendMode.AlphaBlend);//, SpriteSortMode.Deferred, SaveStateMode.None);
            //Draw
            DrawGrid();
            player.Draw();
            for (int i = 0; i < pickups.Count; i++)
            {
                if (pickups[i].alive)
                    pickups[i].Draw();
            }
            Gl.spriteBatch.Draw(Gl.dot, new Rectangle(0, Gl.graphics.Viewport.Height - 64, Gl.graphics.Viewport.Width, 64), Color.Black);
            Gl.spriteBatch.Draw(Gl.dot, new Rectangle(0, Viewport.Bottom - 1, Viewport.Width, 1), themeColor);
            Gl.spriteBatch.Draw(Gl.dot, new Rectangle(Viewport.Width - 1, 0, 1, Viewport.Height), themeColor);
            #region HighScore and Score
            string highScoreText = "Highscore: ";
            if (difficulty == Difficulty.Easy)
                highScoreText += HighScoreManager.EasyHighScore.ToString() + " by " + HighScoreManager.EasyName;
            else if (difficulty == Difficulty.Moderate)
                highScoreText += HighScoreManager.ModerateHighScore.ToString() + " by " + HighScoreManager.ModerateName;
            else if (difficulty == Difficulty.Hard)
                highScoreText += HighScoreManager.HardHighScore.ToString() + " by " + HighScoreManager.HardName;
            else if (difficulty == Difficulty.Insane)
                highScoreText += HighScoreManager.InsaneHighScore.ToString() + " by " + HighScoreManager.InsaneName;
            Gl.spriteBatch.DrawString(Gl.Load<SpriteFont>("SpriteFont1"), "Score: " + score.ToString() + (isCheating ? "CHEATED" : "") + "   " + highScoreText, new Vector2(0, Gl.graphics.Viewport.Height - 25), themeColor);
            #endregion
            if (messageAlpha > 0)
            {
                Gl.spriteBatch.DrawString(Gl.Load<SpriteFont>("BigFont"), message, new Vector2(0,Gl.graphics.Viewport.Height - 59), new Color(themeColor, messageAlpha));
            }
            if (paused)
            {
                Gl.spriteBatch.Draw(Gl.dot, new Rectangle(0, 0, Viewport.Width, Viewport.Height), new Color(Color.Black, 1f));
                Vector2 size = Gl.Load<SpriteFont>("SpriteFont1").MeasureString("Paused... Press [P] to unpause.");
                Gl.spriteBatch.DrawString(Gl.Load<SpriteFont>("SpriteFont1"), "Paused... Press [P] to unpause.", new Vector2(Viewport.Width / 2 - size.X / 2, Viewport.Height / 2 - size.Y / 2), Color.White);
            }
            Gl.spriteBatch.End();
        }
        private static void DrawGrid()
        {
            for (int x = 0; x < gridSize.X; x++)
            {
                Gl.spriteBatch.Draw(Gl.dot, new Rectangle((int)(x * cellSize.X), 0, 1, Viewport.Height), themeColor);
            }
            for (int y = 0; y < gridSize.Y; y++)
            {
                Gl.spriteBatch.Draw(Gl.dot, new Rectangle(0, (int)(y * cellSize.Y), Viewport.Width, 1), themeColor);
            }
        }
    }
}
