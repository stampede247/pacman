﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace Snake
{
    class Player
    {
        static Texture2D headSprite, bodyPieceSprite;
        public Point position;
        public Direction direction;
        public int tailLength;
        public List<Point> tail;
        public bool loop = false;
        public int frameDelayTime = 2;
        public int scoreToGetLooping = 50;
        public int deathTimer;
        public int deathTimeTotal = 60;
        public bool isDieing;
        public int timer;

        public static void LoadContent()
        {
            headSprite = Gl.dot;
            bodyPieceSprite = Gl.dot;
            headSprite = Gl.Load("PacmanHead");
            bodyPieceSprite = Gl.Load("PacmanBody");
        }
        public Player(Point pos, Direction direction, int tailLength)
        {
            this.position = pos;
            this.tail = new List<Point>();
            tail.Add(position);
            this.direction = direction;
            this.tailLength = tailLength;
            isDieing = false;
        }

        public void Update()
        {
            if (!isDieing)
            {
                timer--;
                if (timer <= 0)
                {
                    AddTail();
                    timer = frameDelayTime;
                    #region Movement
                    if (direction == Direction.Down)
                        position.Y += 1;
                    if (direction == Direction.Up)
                        position.Y -= 1;
                    if (direction == Direction.Left)
                        position.X -= 1;
                    if (direction == Direction.Right)
                        position.X += 1;
                    if (position.X < 0 || position.Y < 0 || position.X >= Client.gridSize.X || position.Y >= Client.gridSize.Y)
                    {
                        if (loop)
                        {
                            if (position.X < 0)
                                position.X = Client.gridSize.X - 1;
                            if (position.Y < 0)
                                position.Y = Client.gridSize.Y - 1;
                            if (position.X >= Client.gridSize.X)
                                position.X = 0;
                            if (position.Y >= Client.gridSize.Y)
                                position.Y = 0;
                        }
                        else
                        {
                            Kill();
                        }
                    }
                    #endregion
                    #region Check for Collisions with Tail
                    for (int i = 0; i < tail.Count; i++)
                    {
                        if (tail[i] == position)
                        {
                            Kill();
                            break;
                        }
                    }
                    #endregion
                    #region Check for Pickup
                    for (int i = 0; i < Client.pickups.Count; i++)
                    {
                        if (Client.pickups[i].alive)
                        {
                            if (Client.pickups[i].position == position)
                            {
                                Client.pickups[i].Destroy();
                                Pickup();
                            }
                        }
                    }
                    #endregion
                }
                #region Arrow Keys
                if (Gl.KeyPress(Keys.Right) && direction != Direction.Left && tail[tail.Count - 1] != new Point(position.X + 1, position.Y))
                {
                    direction = Direction.Right;
                }
                if (Gl.KeyPress(Keys.Left) && direction != Direction.Right && tail[tail.Count - 1] != new Point(position.X - 1, position.Y))
                {
                    direction = Direction.Left;
                }
                if (Gl.KeyPress(Keys.Up) && direction != Direction.Down && tail[tail.Count - 1] != new Point(position.X, position.Y - 1))
                {
                    direction = Direction.Up;
                }
                if (Gl.KeyPress(Keys.Down) && direction != Direction.Up && tail[tail.Count - 1] != new Point(position.X, position.Y + 1))
                {
                    direction = Direction.Down;
                }
                #endregion
            }
            else
            {
                #region Handle Death
                deathTimer--;
                if (deathTimer == 0)
                    Client.Reset();
                #endregion
            }
        }
        public void Kill()
        {
            if (Client.score > HighScoreManager.getHighscoreOf(Client.difficulty))
                Gl.game.GotoNewHighScore();
            isDieing = true;
            deathTimer = deathTimeTotal;
        }
        private void Pickup()
        {
            Client.score++;
            Client.AddPickup();
            AddTailLength(Client.difficulty == Difficulty.Insane ? 10 : 1);
            if (Client.score == scoreToGetLooping)
            {
                loop = true;
                Client.ShowMessage("Looping Enabled!!", Client.themeColor);
            }
        }
        private void AddTail()
        {
            tail.Add(position);
            while (tail.Count > tailLength)
            {
                tail.RemoveAt(0);
            }
        }
        public void AddTailLength(int amount)
        {
            tailLength += amount;
        }

        public void Draw()
        {
            if (!isDieing)
            {
                Gl.spriteBatch.Draw(headSprite, new Vector2(Client.cellSize.X * position.X + Client.cellSize.X / 2, Client.cellSize.Y * position.Y + Client.cellSize.Y / 2), null, Color.White, GetRadians(direction), new Vector2(headSprite.Width / 2f, headSprite.Height / 2f), new Vector2(Client.cellSize.X, Client.cellSize.Y) / headSprite.Width, SpriteEffects.None, 0f);
                for (int i = 0; i < tail.Count; i++)
                {
                    Gl.spriteBatch.Draw(bodyPieceSprite, new Rectangle((int)(Client.cellSize.X * tail[i].X), (int)(Client.cellSize.Y * tail[i].Y), (int)(Client.cellSize.X), (int)(Client.cellSize.Y)), Color.White);
                }
            }
            else
            {
                Gl.spriteBatch.Draw(headSprite, new Vector2(Client.cellSize.X * position.X + Client.cellSize.X / 2, Client.cellSize.Y * position.Y + Client.cellSize.Y / 2), null, new Color(Color.White, (float)deathTimer / (float)deathTimeTotal), GetRadians(direction), new Vector2(headSprite.Width / 2f, headSprite.Height / 2f), new Vector2(Client.cellSize.X, Client.cellSize.Y) / headSprite.Width, SpriteEffects.None, 0f);
                for (int i = 0; i < tail.Count; i++)
                {
                    Gl.spriteBatch.Draw(bodyPieceSprite, new Rectangle((int)(Client.cellSize.X * tail[i].X), (int)(Client.cellSize.Y * tail[i].Y), (int)(Client.cellSize.X), (int)(Client.cellSize.Y)), new Color(Color.White, (float)deathTimer / (float)deathTimeTotal));
                }
            }
        }
        public static float GetDegrees(Direction direction)
        {
            if (direction == Direction.Right)
                return 0f;
            if (direction == Direction.Left)
                return 180f;
            if (direction == Direction.Down)
                return 90f;
            else
                return 270f;
        }
        public static float GetRadians(Direction direction)
        {
            if (direction == Direction.Right)
                return 0f;
            if (direction == Direction.Left)
                return MathHelper.Pi;
            if (direction == Direction.Down)
                return MathHelper.PiOver2;
            else
                return MathHelper.PiOver2 * 3;
        }
    }
}
