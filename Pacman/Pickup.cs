﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace Snake
{
    //enum PickupType
    //{
    //    Cherry,
    //    Mushroom,
    //    Bananas,
    //    Strawberry
    //}
    class Pickup
    {
        public static Texture2D spriteSheet;
        public static Point cellSize = new Point(16, 16);
        public Point position;
        public bool alive;
        public Point cell;

        public Pickup(Point position, Point cell)
        {
            this.position = position;
            this.alive = true;
            this.cell = cell;
        }

        public void Destroy()
        {
            this.alive = false;
        }

        public void Draw()
        {
            if (alive)
            {
                Gl.spriteBatch.Draw(spriteSheet, new Rectangle((int)(Client.cellSize.X * position.X), (int)(Client.cellSize.Y * position.Y), (int)Client.cellSize.X, (int)Client.cellSize.Y), new Rectangle(cellSize.X * cell.X, cellSize.Y * cell.Y, cellSize.X, cellSize.Y), Color.White);
            }
        }
    }
}
