﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace Snake
{
    class Calc
    {
        public static Vector2 dirVec(float direction, bool degrees)
        {
            return (new Vector2((float)Math.Cos((degrees) ? MathHelper.ToRadians(direction) : direction), (float)Math.Sin((degrees) ? MathHelper.ToRadians(direction) : direction)));
        }
        public static Vector3 dirVec(float xDir, float zDir, bool degrees)
        {
            float xRot = xDir;
            float zRot = zDir;
            if (degrees)
            {
                xRot = MathHelper.ToRadians(xDir);
                zRot = MathHelper.ToRadians(zDir);
            }
            float z = (float)Math.Sin(zRot);
            float distance = (float)Math.Cos(zRot);
            float x = distance * (float)Math.Cos(xRot);
            float y = distance * (float)Math.Sin(xRot);
            return (new Vector3(x, y, z));
        }
        public static float Pulsate(float min, float max, float seconds)
        {
            float val = Gl.gameTime.TotalRealTime.Milliseconds / 1000.0f;
            while (val > seconds)
            {
                val -= seconds;
            }
            return MathHelper.Lerp(min, max, Math.Abs(val - (seconds / 2)) / seconds);
        }
    }

    class Input
    {
        public static Point MousePos
        {
            get
            {
                return new Point(Gl.mouse.X, Gl.mouse.Y);
            }
        }
        public static Point MousePosLast
        {
            get
            {
                return new Point(Gl.mouseLast.X, Gl.mouseLast.Y);
            }
        }
        public static Vector2 MouseViewPos
        {
            get
            {
                return View.worldPos(new Vector2(Gl.mouse.X, Gl.mouse.Y));
            }
        }
        public static Vector2 MouseViewPosLast
        {
            get
            {
                return View.worldPos(new Vector2(Gl.mouseLast.X, Gl.mouseLast.Y));
            }
        }

        private static MouseState mouse;
        private static KeyboardState keyboard;

        public static Vector2 mouseLeftStart;
        public static Vector2 mouseLeftStartView;
        public static Vector2 mouseRightStart;
        public static Vector2 mouseRightStartView;

        public static void Update()
        {
            mouse = Mouse.GetState();
            keyboard = Keyboard.GetState();

            if (MousePress(true))
            {
                mouseLeftStart = new Vector2(mouse.X, mouse.Y);
                mouseLeftStartView = View.MousePos;
            }
            if (MousePress(false))
            {
                mouseRightStart = new Vector2(mouse.X, mouse.Y);
                mouseRightStartView = View.MousePos;
            }
            if (MouseRelease(true))
            {
                if (Vector2.Distance(mouseLeftStart, new Vector2(mouse.X, mouse.Y)) > 2)
                {

                }
            }
        }

        public static string KeysToString(bool allowEnter)
        {
            bool capital = System.Windows.Forms.Control.IsKeyLocked(System.Windows.Forms.Keys.CapsLock);
            string input = "";
            foreach (Keys key in Gl.keyboard.GetPressedKeys())
            {
                if (Gl.keyboardLast.IsKeyUp(key))
                {
                    if (Gl.KeyDown(Keys.LeftShift) || Gl.KeyDown(Keys.RightShift))
                    {
                        #region Alternate Symbols
                        if (key == Keys.OemBackslash)
                            input += "|";
                        else if (key == Keys.OemCloseBrackets)
                            input += "}";
                        else if (key == Keys.OemComma)
                            input += "<";
                        else if (key == Keys.OemMinus)
                            input += "_";
                        else if (key == Keys.OemOpenBrackets)
                            input += "{";
                        else if (key == Keys.OemPeriod)
                            input += ">";
                        else if (key == Keys.OemPlus)
                            input += "+";
                        else if (key == Keys.OemQuestion)
                            input += "?";
                        else if (key == Keys.OemQuotes)
                            input += '"';
                        else if (key == Keys.OemSemicolon)
                            input += ":";
                        else if (key == Keys.OemTilde)
                            input += "~";
                        else if (key == Keys.D0 || key == Keys.NumPad0)
                            input += ")";
                        else if (key == Keys.D1 || key == Keys.NumPad1)
                            input += "!";
                        else if (key == Keys.D2 || key == Keys.NumPad2)
                            input += "@";
                        else if (key == Keys.D3 || key == Keys.NumPad3)
                            input += "#";
                        else if (key == Keys.D4 || key == Keys.NumPad4)
                            input += "$";
                        else if (key == Keys.D5 || key == Keys.NumPad5)
                            input += "%";
                        else if (key == Keys.D6 || key == Keys.NumPad6)
                            input += "^";
                        else if (key == Keys.D7 || key == Keys.NumPad7)
                            input += "&";
                        else if (key == Keys.D8 || key == Keys.NumPad8)
                            input += "*";
                        else if (key == Keys.D9 || key == Keys.NumPad9)
                            input += "(";
                        #endregion
                    }
                    else
                    {
                        #region Lowercase Symbols
                        if (key == Keys.OemBackslash)
                            input += "\\";
                        else if (key == Keys.OemCloseBrackets)
                            input += "]";
                        else if (key == Keys.OemComma)
                            input += ",";
                        else if (key == Keys.OemMinus)
                            input += "-";
                        else if (key == Keys.OemOpenBrackets)
                            input += "[";
                        else if (key == Keys.OemPeriod)
                            input += ".";
                        else if (key == Keys.OemPlus)
                            input += "=";
                        else if (key == Keys.OemQuestion)
                            input += "/";
                        else if (key == Keys.OemQuotes)
                            input += "'";
                        else if (key == Keys.OemSemicolon)
                            input += ";";
                        else if (key == Keys.OemTilde)
                            input += "`";
                        else if (key == Keys.D0 || key == Keys.NumPad0)
                            input += "0";
                        else if (key == Keys.D1 || key == Keys.NumPad1)
                            input += "1";
                        else if (key == Keys.D2 || key == Keys.NumPad2)
                            input += "2";
                        else if (key == Keys.D3 || key == Keys.NumPad3)
                            input += "3";
                        else if (key == Keys.D4 || key == Keys.NumPad4)
                            input += "4";
                        else if (key == Keys.D5 || key == Keys.NumPad5)
                            input += "5";
                        else if (key == Keys.D6 || key == Keys.NumPad6)
                            input += "6";
                        else if (key == Keys.D7 || key == Keys.NumPad7)
                            input += "7";
                        else if (key == Keys.D8 || key == Keys.NumPad8)
                            input += "8";
                        else if (key == Keys.D9 || key == Keys.NumPad9)
                            input += "9";
                        #endregion
                    }
                    if (capital || Gl.KeyDown(Keys.LeftShift) || Gl.KeyDown(Keys.RightShift))
                    {
                        #region Capital Letters
                        if (key == Keys.A)
                            input += "A";
                        else if (key == Keys.B)
                            input += "B";
                        else if (key == Keys.C)
                            input += "C";
                        else if (key == Keys.D)
                            input += "D";
                        else if (key == Keys.E)
                            input += "E";
                        else if (key == Keys.F)
                            input += "F";
                        else if (key == Keys.G)
                            input += "G";
                        else if (key == Keys.H)
                            input += "H";
                        else if (key == Keys.I)
                            input += "I";
                        else if (key == Keys.J)
                            input += "J";
                        else if (key == Keys.K)
                            input += "K";
                        else if (key == Keys.L)
                            input += "L";
                        else if (key == Keys.M)
                            input += "M";
                        else if (key == Keys.N)
                            input += "N";
                        else if (key == Keys.O)
                            input += "O";
                        else if (key == Keys.P)
                            input += "P";
                        else if (key == Keys.Q)
                            input += "Q";
                        else if (key == Keys.R)
                            input += "R";
                        else if (key == Keys.S)
                            input += "S";
                        else if (key == Keys.T)
                            input += "T";
                        else if (key == Keys.U)
                            input += "U";
                        else if (key == Keys.V)
                            input += "V";
                        else if (key == Keys.W)
                            input += "W";
                        else if (key == Keys.X)
                            input += "X";
                        else if (key == Keys.Y)
                            input += "Y";
                        else if (key == Keys.Z)
                            input += "Z";
                        #endregion
                    }
                    else
                    {
                        #region Lowercase Letters
                        if (key == Keys.A)
                            input += "a";
                        else if (key == Keys.B)
                            input += "b";
                        else if (key == Keys.C)
                            input += "c";
                        else if (key == Keys.D)
                            input += "d";
                        else if (key == Keys.E)
                            input += "e";
                        else if (key == Keys.F)
                            input += "f";
                        else if (key == Keys.G)
                            input += "g";
                        else if (key == Keys.H)
                            input += "h";
                        else if (key == Keys.I)
                            input += "i";
                        else if (key == Keys.J)
                            input += "j";
                        else if (key == Keys.K)
                            input += "k";
                        else if (key == Keys.L)
                            input += "l";
                        else if (key == Keys.M)
                            input += "m";
                        else if (key == Keys.N)
                            input += "n";
                        else if (key == Keys.O)
                            input += "o";
                        else if (key == Keys.P)
                            input += "p";
                        else if (key == Keys.Q)
                            input += "q";
                        else if (key == Keys.R)
                            input += "r";
                        else if (key == Keys.S)
                            input += "s";
                        else if (key == Keys.T)
                            input += "t";
                        else if (key == Keys.U)
                            input += "u";
                        else if (key == Keys.V)
                            input += "v";
                        else if (key == Keys.W)
                            input += "w";
                        else if (key == Keys.X)
                            input += "x";
                        else if (key == Keys.Y)
                            input += "y";
                        else if (key == Keys.Z)
                            input += "z";
                        #endregion
                    }
                    if (key == Keys.Enter && allowEnter)
                    {
                        input += "\n";
                    }
                    else if (key == Keys.Space)
                    {
                        input += " ";
                    }
                }
            }
            return input;
        }
        public static string KeysToNumber()
        {
            string input = "";
            foreach (Keys key in Gl.keyboard.GetPressedKeys())
            {
                if (Gl.keyboardLast.IsKeyUp(key))
                {
                    if (!Gl.KeyDown(Keys.LeftShift) && !Gl.KeyDown(Keys.RightShift))
                    {
                        #region Numbers
                        if (key == Keys.D0 || key == Keys.NumPad0)
                            input += "0";
                        else if (key == Keys.D1 || key == Keys.NumPad1)
                            input += "1";
                        else if (key == Keys.D2 || key == Keys.NumPad2)
                            input += "2";
                        else if (key == Keys.D3 || key == Keys.NumPad3)
                            input += "3";
                        else if (key == Keys.D4 || key == Keys.NumPad4)
                            input += "4";
                        else if (key == Keys.D5 || key == Keys.NumPad5)
                            input += "5";
                        else if (key == Keys.D6 || key == Keys.NumPad6)
                            input += "6";
                        else if (key == Keys.D7 || key == Keys.NumPad7)
                            input += "7";
                        else if (key == Keys.D8 || key == Keys.NumPad8)
                            input += "8";
                        else if (key == Keys.D9 || key == Keys.NumPad9)
                            input += "9";
                        else if (key == Keys.OemMinus)
                            input += "-";
                        #endregion
                    }
                }
            }
            return input;
        }

        public static bool KeyPress(Keys key)
        {
            return (Gl.keyboard.IsKeyDown(key) && !Gl.keyboardLast.IsKeyDown(key));
        }
        public static bool KeyRelease(Keys key)
        {
            return (!Gl.keyboard.IsKeyDown(key) && Gl.keyboardLast.IsKeyDown(key));
        }
        public static bool KeyDown(Keys key)
        {
            return (Gl.keyboard.IsKeyDown(key));
        }

        public static bool MousePress(bool Left)
        {
            if (Left)
            {
                return (Gl.mouse.LeftButton == ButtonState.Pressed && Gl.mouseLast.LeftButton == ButtonState.Released);
            }
            else
            {
                return (Gl.mouse.RightButton == ButtonState.Pressed && Gl.mouseLast.RightButton == ButtonState.Released);
            }
        }
        public static bool MouseRelease(bool Left)
        {
            if (Left)
            {
                return (Gl.mouse.LeftButton == ButtonState.Released && Gl.mouseLast.LeftButton == ButtonState.Pressed);
            }
            else
            {
                return (Gl.mouse.RightButton == ButtonState.Released && Gl.mouseLast.RightButton == ButtonState.Pressed);
            }
        }
        public static bool MousDown(bool Left)
        {
            if (Left)
            {
                return (Gl.mouse.LeftButton == ButtonState.Pressed);
            }
            else
            {
                return (Gl.mouse.RightButton == ButtonState.Pressed);
            }
        }
        public static int MouseScroll
        {
            get
            {
                return (int)((Gl.mouse.ScrollWheelValue - Gl.mouseLast.ScrollWheelValue) / 120);
            }
        }
    }

    class TextBox
    {
        public static bool ActiveBox = false;

        public string text;
        public int cursorPos;
        public Rectangle drawRec;
        public SpriteFont font;
        private int backSpaceTime;
        public bool allowEnter = false;
        public bool onlyNumbers = false;
        public bool isActive;
        private int cursorTimer;
        private int mouseCursorPos;
        public bool viewBased;

        public bool drawBack = true;
        public bool drawOutline = true;
        public Color backColor1 = Color.LightGray;
        public Color backColor2 = Color.White;
        public Color outlineColor1 = Color.Black;
        public Color outlineColor2 = Color.Black;
        public Color textColor1 = Color.Black;
        public Color textColor2 = Color.Black;

        private Point textPos
        {
            get
            {
                return new Point(drawRec.X + 5, drawRec.Center.Y - (int)(font.MeasureString(text).Y / 2));
            }
        }
        Point mousePos
        {
            get
            {
                if (viewBased)
                    return new Point((int)View.MousePos.X, (int)View.MousePos.Y);
                else
                    return Gl.MousePos;
            }
        }

        public TextBox(SpriteFont font, string text, Rectangle drawRec, bool viewBased)
        {
            this.font = font;
            this.text = text;
            this.drawRec = drawRec;
            cursorPos = text.Length;
            backSpaceTime = 0;
            isActive = false;
            this.viewBased = viewBased;
        }
        public TextBox(SpriteFont font, string text, Rectangle drawRec, bool viewBased,
             Color backColor1, Color backColor2, Color outlineColor1, Color outlineColor2, Color textColor1 ,Color textColor2)
        {
            this.font = font;
            this.text = text;
            this.drawRec = drawRec;
            cursorPos = text.Length;
            backSpaceTime = 0;
            isActive = false;
            this.viewBased = viewBased;
            this.backColor1 = backColor1;
            this.backColor2 = backColor2;
            this.outlineColor1 = outlineColor1;
            this.outlineColor2 = outlineColor2;
            this.textColor1 = textColor1;
            this.textColor2 = textColor2;
        }

        public static void UpdateActive()
        {
            ActiveBox = false;
        }

        public void Clear()
        {
            text = "";
            cursorPos = 0;
        }

        public void Update()
        {
            ActiveBox = (isActive) ? true : ActiveBox;
            if (isActive)
            {
                if (Input.MousePress(true) && !drawRec.Contains(mousePos))
                {
                    isActive = false;
                }
                #region MouseCursorPos
                int closest = -1;
                int index = -1;
                for (int i = 0; i <= text.Length; i++)
                {
                    Vector2 size = font.MeasureString(text.Substring(0, i));
                    int distance = (int)Math.Abs((mousePos.X - textPos.X) - size.X);
                    if (distance < closest || closest == -1)
                    {
                        index = i;
                        closest = distance;
                    }
                    else
                    {
                        break;
                    }
                }
                if (index != -1)
                    mouseCursorPos = index;
                #endregion
                string input = (onlyNumbers) ? Input.KeysToNumber() : Input.KeysToString(allowEnter);
                text = text.Insert(cursorPos, input);
                if (input.Length != 0)
                    cursorTimer = 0;
                cursorPos += input.Length;
                if (Gl.KeyDown(Keys.Back))
                    backSpaceTime++;
                else
                    backSpaceTime = 0;
                if ((Gl.KeyPress(Keys.Back) || backSpaceTime > 30) && cursorPos > 0)
                {
                    if (backSpaceTime > 30)
                        backSpaceTime -= 3;
                    text = text.Remove(cursorPos - 1, 1);
                    cursorPos--;
                    backSpaceTime++;
                    if (backSpaceTime > 30)
                    {
                        backSpaceTime -= 5;
                    }
                    cursorTimer = 0;
                }
                if (Gl.KeyPress(Keys.Delete) && cursorPos < text.Length)
                {
                    text = text.Remove(cursorPos, 1);
                    cursorTimer = 0;
                }
                if (Input.MousePress(true) && drawRec.Contains(mousePos))
                {
                    cursorPos = mouseCursorPos;
                    cursorTimer = 0;
                }
                if (Gl.KeyPress(Keys.Left) && cursorPos > 0)
                    cursorPos--;
                if (Gl.KeyPress(Keys.Right) && cursorPos < text.Length)
                    cursorPos++;
                cursorTimer++;
                if (cursorTimer > 60)
                    cursorTimer = 0;
            }
            else
            {
                if (Input.MousePress(true) && drawRec.Contains(mousePos))
                {
                    isActive = true;
                }
            }
        }

        public void Draw()
        {
            Color backColor = (isActive) ? backColor2 : backColor1;
            Color textColor = (isActive) ? textColor2 : textColor1;
            Color outlineColor = (isActive) ? outlineColor2 : outlineColor1;
            if (viewBased)
            {
                View.Draw(Gl.dot, drawRec, backColor);
                View.DrawString(font, text, new Vector2(textPos.X, textPos.Y), textColor);
                Vector2 size = font.MeasureString(text.Substring(0, cursorPos));
                size.Y = font.MeasureString(text).Y;
                if (drawOutline)
                {
                    View.Draw(Gl.dot, new Vector2(drawRec.Left, drawRec.Top), null, outlineColor, 0f, Vector2.Zero, new Vector2(drawRec.Width, View.One));
                    View.Draw(Gl.dot, new Vector2(drawRec.Left, drawRec.Top), null, outlineColor, 0f, Vector2.Zero, new Vector2(View.One, drawRec.Height));
                    View.Draw(Gl.dot, new Vector2(drawRec.Left, drawRec.Bottom - View.One), null, outlineColor, 0f, Vector2.Zero, new Vector2(drawRec.Width, View.One));
                    View.Draw(Gl.dot, new Vector2(drawRec.Right - View.One, drawRec.Top), null, outlineColor, 0f, Vector2.Zero, new Vector2(View.One, drawRec.Height));
                }
                if (isActive && cursorTimer <= 30)
                    View.Draw(Gl.dot, new Rectangle(textPos.X + (int)size.X, textPos.Y, 1, (int)size.Y), Color.Gray);
            }
            else
            {
                Gl.spriteBatch.Draw(Gl.dot, drawRec, backColor);
                Gl.spriteBatch.DrawString(font, text, new Vector2(textPos.X, textPos.Y), textColor);
                Vector2 size = font.MeasureString(text.Substring(0, cursorPos));
                size.Y = font.MeasureString(text).Y;
                if (drawOutline)
                {
                    Gl.spriteBatch.Draw(Gl.dot, new Rectangle(drawRec.Left, drawRec.Top, drawRec.Width, 1), outlineColor);
                    Gl.spriteBatch.Draw(Gl.dot, new Rectangle(drawRec.Left, drawRec.Top, 1, drawRec.Height), outlineColor);
                    Gl.spriteBatch.Draw(Gl.dot, new Rectangle(drawRec.Right - 1, drawRec.Top, 1, drawRec.Height), outlineColor);
                    Gl.spriteBatch.Draw(Gl.dot, new Rectangle(drawRec.Left, drawRec.Bottom - 1, drawRec.Width, 1), outlineColor);
                }
                if (isActive && cursorTimer <= 30)
                    Gl.spriteBatch.Draw(Gl.dot, new Rectangle(textPos.X + (int)size.X, textPos.Y, 1, (int)size.Y), Color.Gray);
            }
        }
    }

    class Button
    {
        public Rectangle drawRec;
        public string text;
        public bool viewBased;
        public SpriteFont font;

        public bool drawOutline = true;
        public Color backColor1 = Color.LightGray;
        public Color backColor2 = Color.White;
        public Color backColor3 = Color.LightCoral;
        public Color outlineColor1 = Color.Black;
        public Color outlineColor2 = Color.Black;
        public Color outlineColor3 = Color.Black;
        public Color textColor1 = Color.Black;
        public Color textColor2 = Color.Black;
        public Color textColor3 = Color.White;
        
        //public event EventHandler Pressed;

        Point mousePos
        {
            get
            {
                if (viewBased)
                    return new Point((int)View.MousePos.X, (int)View.MousePos.Y);
                else
                    return Gl.MousePos;
            }
        }

        public Button(SpriteFont font, Rectangle drawRec, string text, bool viewBased)
        {
            this.drawRec = drawRec;
            this.text = text;
            this.viewBased = viewBased;
            this.font = font;
        }

        public void Update()
        {

        }

        public bool Pressed()
        {
            return (Input.MousePress(true) && drawRec.Contains(mousePos));
        }

        public void Draw()
        {
            bool Hovering = drawRec.Contains(mousePos);
            bool clicking = (drawRec.Contains(mousePos) && Gl.MouseDown(true));
            Color textColor = (Hovering) ? ((clicking) ? textColor3 : textColor2) : textColor1;
            Color backColor = (Hovering) ? ((clicking) ? backColor3 : backColor2) : backColor1;
            Color outlineColor = (Hovering) ? ((clicking) ? outlineColor3 : outlineColor2) : outlineColor1;
            if (viewBased)
            {
                View.Draw(Gl.dot, drawRec, backColor);
                Vector2 size = font.MeasureString(text);
                View.DrawString(font, text, new Vector2(drawRec.Center.X - size.X / 2, drawRec.Center.Y - size.Y / 2), textColor);
                if (drawOutline)
                {
                    View.Draw(Gl.dot, new Vector2(drawRec.Left, drawRec.Top), null, outlineColor, 0f, Vector2.Zero, new Vector2(drawRec.Width, View.One));
                    View.Draw(Gl.dot, new Vector2(drawRec.Left, drawRec.Top), null, outlineColor, 0f, Vector2.Zero, new Vector2(View.One, drawRec.Height));
                    View.Draw(Gl.dot, new Vector2(drawRec.Left, drawRec.Bottom - View.One), null, outlineColor, 0f, Vector2.Zero, new Vector2(drawRec.Width, View.One));
                    View.Draw(Gl.dot, new Vector2(drawRec.Right - View.One, drawRec.Top), null, outlineColor, 0f, Vector2.Zero, new Vector2(View.One, drawRec.Height));
                }
            }
            else
            {
                Gl.spriteBatch.Draw(Gl.dot, drawRec, backColor);
                Vector2 size = font.MeasureString(text);
                Gl.spriteBatch.DrawString(font, text, new Vector2(drawRec.Center.X - size.X / 2, drawRec.Center.Y - size.Y / 2), textColor);
                if (drawOutline)
                {
                    Gl.spriteBatch.Draw(Gl.dot, new Rectangle(drawRec.Left, drawRec.Top, drawRec.Width, 1), outlineColor);
                    Gl.spriteBatch.Draw(Gl.dot, new Rectangle(drawRec.Left, drawRec.Top, 1, drawRec.Height), outlineColor);
                    Gl.spriteBatch.Draw(Gl.dot, new Rectangle(drawRec.Left, drawRec.Bottom - 1, drawRec.Width, 1), outlineColor);
                    Gl.spriteBatch.Draw(Gl.dot, new Rectangle(drawRec.Right - 1, drawRec.Top, 1, drawRec.Height), outlineColor);
                }
            }
        }
    }

    class CheckBox
    {
        public Rectangle drawRec;
        public bool viewBased;
        public bool on;

        public bool drawOutline = true;
        public Color backColor1 = Color.LightGray;
        public Color backColor2 = Color.White;
        public Color outlineColor1 = Color.Black;
        public Color outlineColor2 = Color.Black;
        public Color checkColor = Color.Blue;

        private Point mousePos
        {
            get
            {
                if (viewBased)
                    return new Point((int)View.MousePos.X, (int)View.MousePos.Y);
                else
                    return Input.MousePos;
            }
        }

        public CheckBox(Rectangle drawRec, bool viewBased, bool on)
        {
            this.drawRec = drawRec;
            this.viewBased = viewBased;
            this.on = on;
        }

        public void Update()
        {
            if (Input.MousePress(true) && drawRec.Contains(mousePos))
            {
                on = !on;
            }
        }

        public void Draw()
        {
            Color backColor = (drawRec.Contains(mousePos)) ? backColor2 : backColor1;
            Color outlineColor = (drawRec.Contains(mousePos)) ? outlineColor2 : outlineColor1;
            if (viewBased)
            {
                View.Draw(Gl.dot, drawRec, backColor);
                if (on)
                    View.Draw(Gl.dot, new Rectangle(drawRec.Left + 3, drawRec.Top + 3, drawRec.Width - 6, drawRec.Height - 6), checkColor);
                if (drawOutline)
                {
                    View.Draw(Gl.dot, new Vector2(drawRec.Left, drawRec.Top), null, outlineColor, 0f, Vector2.Zero, new Vector2(drawRec.Width, View.One));
                    View.Draw(Gl.dot, new Vector2(drawRec.Left, drawRec.Top), null, outlineColor, 0f, Vector2.Zero, new Vector2(View.One, drawRec.Height));
                    View.Draw(Gl.dot, new Vector2(drawRec.Left, drawRec.Bottom - View.One), null, outlineColor, 0f, Vector2.Zero, new Vector2(drawRec.Width, View.One));
                    View.Draw(Gl.dot, new Vector2(drawRec.Right - View.One, drawRec.Top), null, outlineColor, 0f, Vector2.Zero, new Vector2(View.One, drawRec.Height));
                }
            }
            else
            {
                Gl.spriteBatch.Draw(Gl.dot, drawRec, backColor);
                if (on)
                    Gl.spriteBatch.Draw(Gl.dot, new Rectangle(drawRec.Left + 2, drawRec.Top + 2, drawRec.Width - 4, drawRec.Height - 4), checkColor);
                if (drawOutline)
                {
                    Gl.spriteBatch.Draw(Gl.dot, new Rectangle(drawRec.Left, drawRec.Top, drawRec.Width, 1), outlineColor);
                    Gl.spriteBatch.Draw(Gl.dot, new Rectangle(drawRec.Left, drawRec.Top,1, drawRec.Height), outlineColor);
                    Gl.spriteBatch.Draw(Gl.dot, new Rectangle(drawRec.Left, drawRec.Bottom - 1, drawRec.Width, 1), outlineColor);
                    Gl.spriteBatch.Draw(Gl.dot, new Rectangle(drawRec.Right - 1, drawRec.Top, 1, drawRec.Height), outlineColor);
                }
            }
        }
    }

    enum Direction
    {
        Left,
        Right,
        Up,
        Down
    }
}
