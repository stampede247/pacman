﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace Snake
{
    enum CInType
    {
        Error,
        Warning,
        Message,
        Variable,
        Success,
        Input,
    }

    class Database
    {
        #region Get Variables
        public static List<string> gVars = new List<string>()
        {
            "",
        };
        #endregion
        #region Set Variables
        public static List<string> sVars = new List<string>()
        {
            "",
        };
        #endregion
        #region Functions
        public static List<string> functions = new List<string>()
        {
            "Reset()","Reset(string difficulty)",
            "SetDelay(int delay)",
            "ToggleLooping()",
        };
        #endregion

        public static string getVariable(string name)
        {
            #region Variables
            if (name == "template")
            {
                return "template";
            }
            #endregion
            return ("THEREISNOVARIABLE");
        }
        public static bool setVariable(string name, string value)
        {
            #region Variables
            //if (name == "reset")
            //{
            //    if (value == "")
            //    {

            //        return true;
            //    }
            //}
            //else if (name == "template")
            //{
            //    if (value == "")
            //    {

            //        return true;
            //    }
            //}
            #endregion
            return (false);
        }

        public static void ExecuteFunction(string name, List<string> vals, int numVals)
        {
            #region Reset
            if (name == "reset")
            {
                if (numVals == 0)
                {
                    Client.Reset();
                    Console.Write("Reset Game on " + Client.difficulty.ToString() + " difficulty.", CInType.Success);
                }
                else if (numVals == 1)
                {
                    if (vals[0].ToLower() == "easy")
                    {
                        Client.difficulty = Difficulty.Easy;
                        Client.Reset();
                        Console.Write("Reset Game on " + Client.difficulty.ToString() + " difficulty.", CInType.Success);
                    }
                    else if (vals[0].ToLower() == "moderate")
                    {
                        Client.difficulty = Difficulty.Moderate;
                        Client.Reset();
                        Console.Write("Reset Game on " + Client.difficulty.ToString() + " difficulty.", CInType.Success);
                    }
                    else if (vals[0].ToLower() == "hard")
                    {
                        Client.difficulty = Difficulty.Hard;
                        Client.Reset();
                        Console.Write("Reset Game on " + Client.difficulty.ToString() + " difficulty.", CInType.Success);
                    }
                    else if (vals[0].ToLower() == "insane")
                    {
                        Client.difficulty = Difficulty.Insane;
                        Client.Reset();
                        Console.Write("Reset Game on " + Client.difficulty.ToString() + " difficulty.", CInType.Success);
                    }
                    else
                    {
                        Console.Write("String variable needs to be 'easy' 'moderate' 'hard' or 'insane'.", CInType.Error);
                    }
                }
                else
                {
                    Console.Write("Cannot Execute: " + name, CInType.Error);
                    Console.Write("Reset() : Reset(string DiffucultyLevel)", CInType.Error);
                }
            }
            #endregion
            #region Set Delay
            else if (name == "setdelay")
            {
                if (numVals == 1)
                {
                    try
                    {
                        int var = int.Parse(vals[0]);
                        Client.player.frameDelayTime = var;
                        Client.TurnOnCheating();
                    }
                    catch(Exception)
                    {
                        Console.Write("Cannot Execute: " + name, CInType.Error);
                        Console.Write("SetDelay(int delay)", CInType.Error);
                    }
                }
                else
                {
                    Console.Write("Cannot Execute: " + name, CInType.Error);
                    Console.Write("SetDelay(int delay)", CInType.Error);
                }
            }
            #endregion
            #region Toggle Looping
            else if (name == "togglelooping")
            {
                if (numVals == 0)
                {
                    if (Client.player.loop == false)
                        Client.ShowMessage("Looping Enabled!!", Client.themeColor);
                    Client.player.loop = !Client.player.loop;
                    Client.TurnOnCheating();
                    Console.Write("Looping is " + (Client.player.loop ? "on" : "off") + ".", CInType.Warning);
                }
                else
                {
                    Console.Write("Cannot Execute: " + name, CInType.Error);
                    Console.Write("ToggleLooping()", CInType.Error);
                }
            }
            #endregion
            else
            {
                Console.Write("Unkown Function: " + name, CInType.Error);
            }
        }
    }

    class Console
    {
        public static Vector2 size = new Vector2(800, 500);
        public static float fontHeight = 15f;
        public static Color inputColor = new Color(150, 250, 150);

        public static List<ConsoleLine> pastLines = new List<ConsoleLine>();
        public static SpriteFont font;
        public static TextBox input;

        public static bool open = false;
        public static void Open()
        {
            open = true;
            input.isActive = true;
        }
        public static void Close()
        {
            open = false;
        }
        public static void Toggle()
        {
            if (open)
                Close();
            else
                Open();
        }

        public static void ChangedClientBounds(Rectangle clientBounds)
        {
            size = new Vector2(clientBounds.Width, clientBounds.Height * (7.0f / 8.0f));
        }

        public static void LoadContent()
        {
            font = Gl.Content.Load<SpriteFont>("ConsoleFont");
            input = new TextBox(font, "", new Rectangle((int)(5), (int)(size.Y - fontHeight - 5), (int)(size.X - 10), (int)fontHeight), false,
                new Color(15,15,15, 127), new Color(Color.Black, 0.5f), Color.Black, Color.Black, Color.LightGray, inputColor);
            input.drawOutline = false;
        }

        public static void Update()
        {
            if (open)
            {
                if (!Input.KeyPress(Keys.OemTilde))
                    input.Update();
                if (Gl.KeyPress(Keys.Enter))
                {
                    Write(input.text, CInType.Input);
                    ConsoleTranslator.Execute(input.text);
                    input.text = "";
                    input.cursorPos = 0;
                }
                if (Gl.KeyPress(Keys.Up))
                {
                    for (int i = pastLines.Count - 1; i >= 0; i--)
                    {
                        ConsoleLine line = pastLines[i];
                        if (line.color == inputColor)
                        {
                            input.text = line.line;
                            input.cursorPos = input.text.Length;
                            break;
                        }
                    }
                }
            }
        }

        public static void Draw()
        {
            if (open)
            {
                Color color1 = new Color(Color.Gray, 0.5f);
                Color color2 = new Color(Color.Black, 0.7f);
                Gl.spriteBatch.Begin();
                //main container
                Gl.spriteBatch.Draw(Gl.dot, new Rectangle(0, 0, (int)size.X, (int)size.Y), color1);
                //Input container
                //Gl.spriteBatch.Draw(Gl.dot, new Rectangle((int)(5), (int)(size.Y - fontHeight - 5), (int)(size.X - 10), (int)fontHeight), color2);
                //Output container
                Gl.spriteBatch.Draw(Gl.dot, new Rectangle((int)(5), (int)(5), (int)(size.X - 10), (int)(size.Y - 15 - fontHeight)), color2);
                input.Draw();//new Vector2(5, size.Y - 5 - fontHeight), inputColor, true);
                int max = pastLines.Count - 1;
                for (int i = max; i > -1; i--)
                {
                    if (pastLines[i].color == inputColor)
                        Gl.spriteBatch.DrawString(font, "] " + pastLines[i].line, new Vector2(10, size.Y - 10 - fontHeight * 2 - ((fontHeight - 5) * (max - i))), pastLines[i].color);
                    else
                        Gl.spriteBatch.DrawString(font, "> " + pastLines[i].line, new Vector2(10, size.Y - 10 - fontHeight * 2 - ((fontHeight - 5) * (max - i))), pastLines[i].color);
                }
                //Top overlay
                Gl.spriteBatch.Draw(Gl.dot, new Rectangle(0, 0, (int)size.X, 5), color1);
                Gl.spriteBatch.End();
            }
        }

        public static void Write(string line, CInType type)
        {
            Color color = Color.White;
            if (type == CInType.Error)
                color = Color.Red;
            else if (type == CInType.Input)
                color = inputColor;
            else if (type == CInType.Message)
                color = Color.LightGray;
            else if (type == CInType.Success)
                color = Color.Green;
            else if (type == CInType.Warning)
                color = Color.Yellow;
            pastLines.Add(new ConsoleLine(line, color));
        }
    }

    struct ConsoleLine
    {
        public string line;
        public Color color;

        public ConsoleLine(string line, Color color)
        {
            this.line = line;
            this.color = color;
        }
    }

    class ConsoleTranslator
    {
        public static void Execute(string line)
        {
            if (line == "help")
            {
                Console.Write("-----Functions-----", CInType.Message);
                Console.Write("", CInType.Message);
                for (int i = 0; i < Database.functions.Count; i++)
                {
                    Console.Write(Database.functions[i], CInType.Message);
                }
                Console.Write("", CInType.Message);
                Console.Write("", CInType.Message);
                Console.Write("-----Settable Variables-----", CInType.Message);
                Console.Write("", CInType.Message);
                for (int i = 0; i < Database.sVars.Count; i++)
                {
                    Console.Write(Database.sVars[i], CInType.Message);
                }
                Console.Write("", CInType.Message);
                Console.Write("", CInType.Message);
                Console.Write("-----Gettable Variables-----", CInType.Message);
                Console.Write("", CInType.Message);
                for (int i = 0; i < Database.gVars.Count; i++)
                {
                    Console.Write(Database.gVars[i], CInType.Message);
                }
            }
            else
            {
                List<string> chunks = new List<string>();
                List<char> seperators = new List<char>();
                int lastSep = 0;
                #region Find Seperators and Chunks
                for (int i = 0; i < line.Length + 1; i++)
                {
                    if (i == line.Length)
                    {
                        if (lastSep != i)
                        {
                            chunks.Add(line.Substring(lastSep, i - lastSep));
                        }
                    }
                    else if (line[i] == ' ' || line[i] == '(' || line[i] == ')' && (lastSep != i || line[i] == ')'))
                    {
                        seperators.Add(line[i]);
                        chunks.Add(line.Substring(lastSep, i - lastSep));
                        lastSep = i + 1;
                    }
                }
                #endregion
                if (chunks.Count == 1 && seperators.Count == 0)
                {
                    //Retrieve a Variable
                    string value = Database.getVariable(chunks[0]);
                    if (value != "THEREISNOVARIABLE")
                    {
                        Console.Write(chunks[0] + " = " + value, CInType.Variable);
                    }
                    else
                    {
                        Console.Write("There is no variable named: " + chunks[0], CInType.Error);
                    }
                }
                else if (chunks.Count == 2)
                {
                    //Function or Setting a Variable
                    if (seperators.Count == 2)
                    {
                        if (seperators[0] == '(' && seperators[1] == ')')
                        {
                            //Function
                            List<string> vals = getStringsBetween(chunks[1], ',');
                            Database.ExecuteFunction(chunks[0].ToLower(), vals, vals.Count);
                        }
                        else
                            Console.Write("Cannot Execute: " + line + "(2 chunks, 2 seperators, not function)", CInType.Error);
                    }
                    else if (seperators.Count == 1)
                    {
                        if (seperators[0] == ' ' || seperators[0] == '=')
                        {
                            if (Database.setVariable(chunks[0].ToLower(), chunks[1]))
                            {
                                Console.Write("Success!: " + chunks[0] + " = " + chunks[1], CInType.Success);
                            }
                            else
                            {
                                Console.Write("Cannot find variable: " + chunks[0], CInType.Error);
                            }
                        }
                        else
                            Console.Write("Cannot Execute: " + line + "(2 chunks, 1 seperator, not setting)", CInType.Error);
                    }
                    else
                        Console.Write("Cannot Execute: " + line + " (2 chunks, too many seperators)", CInType.Error);
                }
                else
                {
                    Console.Write("Cannot Execute: " + line + " (" + chunks.Count.ToString() + " chunks, " + seperators.Count.ToString() + " seperators)", CInType.Error);
                }
            }
        }

        public static List<string> getStringsBetween(string line, char seperator)
        {
            List<string> vals = new List<string>();
            int lastSep = 0;
            for (int i = 0; i < line.Length + 1; i++)
            {
                if (i == line.Length)
                {
                    if (lastSep != i)
                    {
                        vals.Add(line.Substring(lastSep, i - lastSep));
                    }
                }
                else if (line[i] == seperator)
                {
                    vals.Add(line.Substring(lastSep, i - lastSep));
                    lastSep = i + 1;
                }
            }
            return vals;
        }
    }
}
