﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace Snake
{
    class Gl
    {
        public static Game1 game;
        public static SpriteBatch spriteBatch;
        public static GraphicsDevice graphics;
        public static ContentManager Content;

        public static KeyboardState keyboard;
        public static KeyboardState keyboardLast;
        public static MouseState mouse;
        public static MouseState mouseLast;
        public static Point MousePos { get { return new Point(mouse.X, mouse.Y); } }
        public static GamePadState gamepad;
        public static GamePadState gamepadLast;
        public static GameTime gameTime;

        public static Random rand = new Random();
        public static Texture2D dot;
        //public static BasicEffect bEffect;

        public static void LoadContent()
        {
            dot = new Texture2D(graphics, 1, 1);
            dot.SetData<Color>(new Color[1] { Color.White });
            View.Initialize();
            MenuSystem.LoadContent();
            Client.LoadContent();
            Console.LoadContent();
        }
        #region Helper Functions
        private static BasicEffect CreateBasicEffect(bool textured, bool colored)
        {
            BasicEffect bEffect = new BasicEffect(graphics, null);
            bEffect.FogEnabled = false;
            bEffect.LightingEnabled = false;
            bEffect.TextureEnabled = textured;
            bEffect.VertexColorEnabled = colored;

            return bEffect;
        }
        public static void UpdateBefore(GameTime gametime)
        {
            keyboard = Keyboard.GetState();
            mouse = Mouse.GetState();
            gamepad = GamePad.GetState(PlayerIndex.One);
            gameTime = gametime;
            TextBox.UpdateActive();
        }
        public static void UpdateAfter()
        {
            View.Update();
            keyboardLast = keyboard;
            mouseLast = mouse;
            gamepad = gamepadLast;
        }
        public static bool KeyDown(Keys key)
        {
            return keyboard.IsKeyDown(key);
        }
        public static bool KeyPress(Keys key)
        {
            return (keyboard.IsKeyDown(key) && !keyboardLast.IsKeyDown(key));
        }
        public static bool KeyRelease(Keys key)
        {
            return (!keyboard.IsKeyDown(key) && keyboardLast.IsKeyDown(key));
        }
        public static bool MouseDown(bool left)
        {
            if (left)
                return mouse.LeftButton == ButtonState.Pressed;
            else
                return mouse.RightButton == ButtonState.Pressed;
        }
        public static bool MousePress(bool left)
        {
            if (left)
                return (mouse.LeftButton == ButtonState.Pressed && mouseLast.LeftButton != ButtonState.Pressed);
            else
                return (mouse.RightButton == ButtonState.Pressed && mouseLast.RightButton != ButtonState.Pressed);
        }
        public static bool MouseRelease(bool left)
        {
            if (left)
                return (mouse.LeftButton != ButtonState.Pressed && mouseLast.LeftButton == ButtonState.Pressed);
            else
                return (mouse.RightButton != ButtonState.Pressed && mouseLast.RightButton == ButtonState.Pressed);
        }
        public static bool ButtonDown(Buttons button)
        {
            return gamepad.IsButtonDown(button);
        }
        public static bool ButtonPress(Buttons button)
        {
            return (gamepad.IsButtonDown(button) && !gamepadLast.IsButtonDown(button));
        }
        public static bool ButtonRelease(Buttons button)
        {
            return (!gamepad.IsButtonDown(button) && gamepadLast.IsButtonDown(button));
        }
        public static T Load<T>(string path)
        {
            return Content.Load<T>(path);
        }
        public static Texture2D Load(string path)
        {
            return Content.Load<Texture2D>(path);
        }
        public static void DrawRec(Rectangle rec, Color color)
        {
            DrawRec(rec, color, false, false, Color.Black, 1);
        }
        public static void DrawRec(Rectangle rec, Color color, bool view)
        {
            DrawRec(rec, color, view, false, Color.Black, 1);
        }
        public static void DrawRec(Rectangle rec, Color color, bool view, bool outlined)
        {
            DrawRec(rec, color, view, outlined, Color.Black, 1);
        }
        public static void DrawRec(Rectangle rec, Color color, bool view, bool outlined, Color outlineColor)
        {
            DrawRec(rec, color, view, outlined, outlineColor, 1);
        }
        public static void DrawRec(Rectangle rec, Color color, bool view, bool outlined, Color outlineColor, int outlineWidth)
        {
            if (view)
            {
                View.Draw(dot, rec, color);
                if (outlined)
                {
                    View.Draw(dot, new Rectangle(rec.Left, rec.Top, outlineWidth, rec.Height), outlineColor);
                    View.Draw(dot, new Rectangle(rec.Left, rec.Top, rec.Width, outlineWidth), outlineColor);
                    View.Draw(dot, new Rectangle(rec.Right - outlineWidth, rec.Top, outlineWidth, rec.Height), outlineColor);
                    View.Draw(dot, new Rectangle(rec.Left, rec.Bottom - outlineWidth, rec.Width, outlineWidth), outlineColor);
                }
            }
            else
            {
                spriteBatch.Draw(dot, rec, color);
                if (outlined)
                {
                    spriteBatch.Draw(dot, new Rectangle(rec.Left, rec.Top, outlineWidth, rec.Height), outlineColor);
                    spriteBatch.Draw(dot, new Rectangle(rec.Left, rec.Top, rec.Width, outlineWidth), outlineColor);
                    spriteBatch.Draw(dot, new Rectangle(rec.Right - outlineWidth, rec.Top, outlineWidth, rec.Height), outlineColor);
                    spriteBatch.Draw(dot, new Rectangle(rec.Left, rec.Bottom - outlineWidth, rec.Width, outlineWidth), outlineColor);
                }
            }
        }
        public static Color RandomColor()
        {
            return new Color((byte)rand.Next(0, 255), (byte)rand.Next(0, 255), (byte)rand.Next(0, 255), 255);
        }
        public static int Random(int min, int max)
        {
            return (int)(MathHelper.Lerp(min, max, (float)Gl.rand.NextDouble()) + 0.5f);
        }
        public static float Random(float min, float max)
        {
            return MathHelper.Lerp(min, max, (float)Gl.rand.NextDouble());
        }
        public static void DrawTextCentered(SpriteFont font, Vector2 position, string text, Color color)
        {
            Vector2 size = font.MeasureString(text);
            Gl.spriteBatch.DrawString(font, text, position - size / 2, color);
        }
        public static void DrawTextFromRightBottom(SpriteFont font, Vector2 position, string text, Color color)
        {
            Vector2 size = font.MeasureString(text);
            Gl.spriteBatch.DrawString(font, text, position - size, color);
        }
        #endregion
    }
}
