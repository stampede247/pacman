using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace Snake
{
    class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        bool paused = false;
        public bool inMenu;
        float introTimer = 200;
        bool isInIntro = true;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            float screenWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            float screenHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            float scale = 400f / screenHeight;
            graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            graphics.IsFullScreen = true;
            graphics.ApplyChanges();
            this.IsMouseVisible = true;

        }

        protected override void Initialize()
        {

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            Gl.spriteBatch = spriteBatch;
            Gl.Content = Content;
            Gl.graphics = GraphicsDevice;
            Gl.game = this;
            Gl.LoadContent();

            //HighScoreManager.EasyHighScore = 1;
            //HighScoreManager.EasyName = "Taylor";
            //HighScoreManager.ModerateHighScore = 1;
            //HighScoreManager.ModerateName = "Taylor";
            //HighScoreManager.HardHighScore = 1;
            //HighScoreManager.HardName = "Taylor";
            //HighScoreManager.InsaneHighScore = 1;
            //HighScoreManager.InsaneName = "Taylor";
            //HighScoreManager.SaveScores();
            HighScoreManager.LoadScores();
        }
        public void GotoMenu(MenuState state)
        {
            inMenu = true;
            if (state == MenuState.Main)
            {
                MenuSystem.InitializeMain();
            }
            else if (state == MenuState.DifficultySelect)
            {
                MenuSystem.InitializeDifficultySelect();
            }
            else if (state == MenuState.InGame)
            {
                MenuSystem.InitializeInGame();
            }
            else if (state == MenuState.HighScore)
            {
                MenuSystem.InitializeHighScore();
            }
        }
        public void GotoNewHighScore()
        {
            GotoMenu(MenuState.HighScore);
            MenuSystem.EnterNewHighScore(Client.score, Client.difficulty);
        }
        public void GotoGame(Difficulty difficultyLevel)
        {
            inMenu = false;
            Client.Initialize(difficultyLevel);
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();
            paused = !this.IsActive;
            View.position = Vector2.Zero;
            if (isInIntro)
            {
                introTimer--;
                if (introTimer <= 0)
                {
                    isInIntro = false;
                    GotoMenu(MenuState.Main);
                }
            }
            if (!isInIntro)
            {
                if (!paused)
                {
                    Gl.UpdateBefore(gameTime);

                    if (Gl.KeyPress(Keys.OemTilde))
                    {
                        Console.Toggle();
                    }
                    Client.paused = Console.open ? true : Client.paused;
                    MenuSystem.paused = Console.open ? true : MenuSystem.paused;
                    if (inMenu)
                    {
                        MenuSystem.Update();
                    }
                    else
                    {
                        Client.Update();
                    }
                    Console.Update();

                    Gl.UpdateAfter();
                }
            }
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            if (isInIntro)
            {
                Gl.spriteBatch.Begin();
                Texture2D logo = Gl.Load("Logo1");
                Gl.spriteBatch.Draw(logo, new Rectangle(Gl.graphics.Viewport.Width / 2 - logo.Width / 2, Gl.graphics.Viewport.Height / 2 - logo.Height / 2, logo.Width, logo.Height), new Color(Color.White, 3.5f * (introTimer / 200f)));
                Gl.spriteBatch.End();
            }
            else
            {
                if (inMenu)
                {
                    MenuSystem.Draw();
                }
                else
                {
                    Client.Draw();
                }
                Console.Draw();
            }
            Gl.spriteBatch.Begin();
            if (paused)
            {
                Gl.spriteBatch.Draw(Gl.dot, new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height), new Color(Color.Black, 0.3f));
            }
            Gl.spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
