﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using System.IO;

namespace Snake
{
    class HighScoreManager
    {
        public static float EasyHighScore, ModerateHighScore, HardHighScore, InsaneHighScore;
        public static string EasyName, ModerateName, HardName, InsaneName;

        public static void LoadScores()
        {
            if (File.Exists(Gl.Content.RootDirectory + "\\file"))
            {
                FileStream stream = new FileStream(Gl.Content.RootDirectory + "\\file", FileMode.Open);
                using (BinaryReader reader = new BinaryReader(stream))
                {
                    EasyName = ReadString(reader);
                    EasyHighScore = reader.ReadInt16();

                    ModerateName = ReadString(reader);
                    ModerateHighScore = reader.ReadInt16();

                    HardName = ReadString(reader);
                    HardHighScore = reader.ReadInt16();

                    InsaneName = ReadString(reader);
                    InsaneHighScore = reader.ReadInt16();
                }
            }
            else
            {
                EasyHighScore = 0; ModerateHighScore = 0; HardHighScore = 0; InsaneHighScore = 0;
                EasyName = "Undefined"; ModerateName = "Undefinded"; HardName = "Undefined"; InsaneName = "Undefined";
                SaveScores();
            }
        }

        public static void SaveScores()
        {
            FileStream stream = new FileStream(Gl.Content.RootDirectory + "\\file", FileMode.Create);
            using (BinaryWriter writer = new BinaryWriter(stream))
            {
                WriteString(EasyName, writer);
                writer.Write((Int16)EasyHighScore);

                WriteString(ModerateName, writer);
                writer.Write((Int16)ModerateHighScore);

                WriteString(HardName, writer);
                writer.Write((Int16)HardHighScore);

                WriteString(InsaneName, writer);
                writer.Write((Int16)InsaneHighScore);
            }
        }
        private static void WriteString(string str, BinaryWriter writer)
        {
            writer.Write(str.Length);
            for (int i = 0; i < str.Length; i++)
            {
                writer.Write(str[i]);
                writer.Write(Convert.ToChar(Gl.Random(65, 91)));
                writer.Write(Gl.rand.Next());
            }
        }
        private static string ReadString(BinaryReader reader)
        {
            int length = reader.ReadInt32();
            string val = "";
            for (int i = 0; i < length; i++)
            {
                val += reader.ReadChar();
                reader.ReadChar();
                reader.ReadInt32();
            }
            return val;
        }

        public static string getNameOf(Difficulty difficulty)
        {
            if (difficulty == Difficulty.Easy)
                return EasyName;
            else if (difficulty == Difficulty.Moderate)
                return ModerateName;
            else if (difficulty == Difficulty.Hard)
                return HardName;
            else
                return InsaneName;
        }
        public static float getHighscoreOf(Difficulty difficulty)
        {
            if (difficulty == Difficulty.Easy)
                return EasyHighScore;
            else if (difficulty == Difficulty.Moderate)
                return ModerateHighScore;
            else if (difficulty == Difficulty.Hard)
                return HardHighScore;
            else
                return InsaneHighScore;
        }
        public static void setNameOf(Difficulty difficulty, string name)
        {
            if (difficulty == Difficulty.Easy)
                EasyName = name;
            else if (difficulty == Difficulty.Moderate)
                ModerateName = name;
            else if (difficulty == Difficulty.Hard)
                HardName = name;
            else
                InsaneName = name;
        }
        public static void setHighscoreOf(Difficulty difficulty, float score)
        {
            if (difficulty == Difficulty.Easy)
                EasyHighScore = score;
            else if (difficulty == Difficulty.Moderate)
                ModerateHighScore = score;
            else if (difficulty == Difficulty.Hard)
                HardHighScore = score;
            else
                InsaneHighScore = score;
        }
    }
}
