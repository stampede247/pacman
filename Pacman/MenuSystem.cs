﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace Snake
{
    enum MenuState
    {
        Main,
        DifficultySelect,
        InGame,
        HighScore
    }
    class MenuSystem
    {
        public static bool paused = false;
        public static MenuState state;
        public static Rectangle Viewport = new Rectangle(0, 0, Gl.graphics.Viewport.Width, Gl.graphics.Viewport.Height);
        #region Main
        static List<Button> mainButtons;
        static SpriteFont mainFont;
        #endregion
        #region Difficulty Select
        static List<Button> dffButtons;
        static SpriteFont dffFont;
        #endregion
        #region In Game
        static Rectangle menuRec;
        static Color backColor = Color.DarkGray;
        static List<Button> inGameButtons;
        static SpriteFont inGameFont;
        public static bool showScore;
        public static float score;
        #endregion
        #region High Score
        static List<Button> highscoreButtons;
        static SpriteFont highscoreFont;
        static bool enterHighScore;
        static Difficulty enterDiffucultyLevel;
        static float enterScore;
        static TextBox enterTextBox;
        #endregion

        public static void LoadContent()
        {
            mainFont = Gl.Load<SpriteFont>("SpriteFont1");
            dffFont = Gl.Load<SpriteFont>("SpriteFont1");
            inGameFont = Gl.Load<SpriteFont>("SpriteFont1");
            highscoreFont = Gl.Load<SpriteFont>("SpriteFont1");
        }

        public static void InitializeMain()
        {
            state = MenuState.Main;
            mainButtons = new List<Button>();
            Button button = new Button(mainFont, new Rectangle(Viewport.Width / 2 - 100, Viewport.Height / 2 - 142, 200, 85), "Start Game", false);
            mainButtons.Add(button);
            button = new Button(mainFont, new Rectangle(Viewport.Width / 2 - 100, Viewport.Height / 2 - 42, 200, 85), "High Scores", false);
            mainButtons.Add(button);
            button = new Button(mainFont, new Rectangle(Viewport.Width / 2 - 100, Viewport.Height / 2 + 58, 200, 85), "Exit", false);
            mainButtons.Add(button);

        }
        public static void InitializeDifficultySelect()
        {
            state = MenuState.DifficultySelect;
            dffButtons = new List<Button>();
            Button button = new Button(dffFont, new Rectangle(Viewport.Width / 2 - 100, Viewport.Height / 2 - 42 - 150, 200, 85), "Easy", false)
            {
                backColor1 = Color.DarkGray,
                backColor2 = Color.Gray,
                backColor3 = Color.LightGray,
                textColor1 = Color.White,
                textColor2 = Color.White,
                textColor3 = Color.White
            };
            dffButtons.Add(button);
            button = new Button(dffFont, new Rectangle(Viewport.Width / 2 - 100, Viewport.Height / 2 - 42 - 50, 200, 85), "Moderate", false)
            {
                backColor1 = Color.DarkGray,
                backColor2 = Color.Gray,
                backColor3 = Color.LightGray,
                textColor1 = Color.Yellow,
                textColor2 = Color.Yellow,
                textColor3 = Color.Yellow
            };
            dffButtons.Add(button);
            button = new Button(dffFont, new Rectangle(Viewport.Width / 2 - 100, Viewport.Height / 2 - 42 + 50, 200, 85), "Hard", false)
            {
                backColor1 = Color.DarkGray,
                backColor2 = Color.Gray,
                backColor3 = Color.LightGray,
                textColor1 = Color.Red,
                textColor2 = Color.Red,
                textColor3 = Color.Red
            };
            dffButtons.Add(button);
            button = new Button(dffFont, new Rectangle(Viewport.Width / 2 - 100, Viewport.Height / 2 - 42 + 150, 200, 85), "Insane", false)
            {
                backColor1 = Color.DarkGray,
                backColor2 = Color.Gray,
                backColor3 = Color.LightGray,
                textColor1 = Color.OrangeRed,
                textColor2 = Color.OrangeRed,
                textColor3 = Color.OrangeRed
            };
            dffButtons.Add(button);
        }
        public static void InitializeInGame()
        {
            state = MenuState.InGame;

            showScore = false;
            menuRec = new Rectangle(Viewport.Width / 2 - 200, Viewport.Height / 2 - 150, 450, 200);
            inGameButtons = new List<Button>();
            Button button = new Button(inGameFont, new Rectangle(menuRec.X + 5, menuRec.Bottom - 5 - 50, 130, 50), "Return", false);
            inGameButtons.Add(button);
            button = new Button(inGameFont, new Rectangle(menuRec.X + 130 + 10, menuRec.Bottom - 5 - 50, 170, 50), "Change Difficulty", false);
            inGameButtons.Add(button);
            button = new Button(inGameFont, new Rectangle(menuRec.X + 300 + 15, menuRec.Bottom - 5 - 50, 130, 50), "Main Menu", false);
            inGameButtons.Add(button);

        }
        public static void InitializeHighScore()
        {
            state = MenuState.HighScore;
            enterHighScore = false;

            highscoreButtons = new List<Button>();
            Button button = new Button(highscoreFont, new Rectangle(Viewport.Width / 2 - 100, Viewport.Height / 2 - 42 + 200, 200, 85), "Return", false);
            highscoreButtons.Add(button);
        }
        public static void EnterNewHighScore(float score, Difficulty difficulty)
        {
            InitializeHighScore();
            highscoreButtons[0] = new Button(highscoreFont, new Rectangle(Viewport.Width / 2 - 100, Viewport.Height / 2 - 42 + 200, 200, 85), "Save", false);
            enterHighScore = true;
            enterScore = score;
            enterDiffucultyLevel = difficulty;
            int y;
            if (difficulty == Difficulty.Easy)
                y = 230;
            else if (difficulty == Difficulty.Moderate)
                y = 300;
            else if (difficulty == Difficulty.Hard)
                y = 370;
            else
                y = 460;
            enterTextBox = new TextBox(highscoreFont, HighScoreManager.getNameOf(difficulty), new Rectangle(Viewport.Width / 2, y, 200, 21), false);
        }

        public static void Update()
        {
            if (state == MenuState.Main)
                UpdateMain();
            else if (state == MenuState.DifficultySelect)
                UpdateDifficultySelect();
            else if (state == MenuState.InGame)
                UpdateInGame();
            else if (state == MenuState.HighScore)
                UpdateInHighScore();
        }
        private static void UpdateMain()
        {
            for (int i = 0; i < mainButtons.Count; i++)
            {
                mainButtons[i].Update();
                if (mainButtons[i].Pressed())
                {
                    if (i == 0)
                    {
                        Gl.game.GotoMenu(MenuState.DifficultySelect);
                    }
                    else if (i == 1)
                    {
                        Gl.game.GotoMenu(MenuState.HighScore);
                        //EnterNewHighScore(0, Difficulty.Hard);
                    }
                    else if (i == 2)
                    {
                        Gl.game.Exit();
                    }
                }
            }
        }
        private static void UpdateDifficultySelect()
        {
            if (Gl.KeyPress(Keys.Escape) || Gl.KeyPress(Keys.Back) || Gl.KeyPress(Keys.Tab))
            {
                Gl.game.GotoMenu(MenuState.Main);
            }
            for (int i = 0; i < dffButtons.Count; i++)
            {
                dffButtons[i].Update();
                if (dffButtons[i].Pressed())
                {
                    if (i == 0)
                    {
                        Gl.game.GotoGame(Difficulty.Easy);
                    }
                    else if (i == 1)
                    {
                        Gl.game.GotoGame(Difficulty.Moderate);
                    }
                    else if (i == 2)
                    {
                        Gl.game.GotoGame(Difficulty.Hard);
                    }
                    else if (i == 3)
                    {
                        Gl.game.GotoGame(Difficulty.Insane);
                    }
                }
            }
        }
        private static void UpdateInGame()
        {
            if (Gl.KeyPress(Keys.Escape) || Gl.KeyPress(Keys.Back) || Gl.KeyPress(Keys.Tab))
                Gl.game.inMenu = false;
            for (int i = 0; i < inGameButtons.Count; i++)
            {
                inGameButtons[i].Update();
                if (inGameButtons[i].Pressed())
                {
                    if (i == 0)
                    {
                        Gl.game.inMenu = false;
                    }
                    else if (i == 1)
                    {
                        Gl.game.GotoMenu(MenuState.DifficultySelect);
                    }
                    else if (i == 2)
                    {
                        Gl.game.GotoMenu(MenuState.Main);
                    }
                }
            }
        }
        private static void UpdateInHighScore()
        {
            if ((Gl.KeyPress(Keys.Escape) || Gl.KeyPress(Keys.Back) || Gl.KeyPress(Keys.Tab)) && !enterTextBox.isActive)
                Gl.game.GotoMenu(MenuState.Main);
            for (int i = 0; i < highscoreButtons.Count; i++)
            {
                highscoreButtons[i].Update();
                if (highscoreButtons[i].Pressed())
                {
                    if (i == 0)
                    {
                        if (enterHighScore)
                        {
                            HighScoreManager.setNameOf(enterDiffucultyLevel, enterTextBox.text);
                            HighScoreManager.setHighscoreOf(enterDiffucultyLevel, enterScore);
                            HighScoreManager.SaveScores();
                        }
                        Gl.game.GotoMenu(MenuState.Main);
                    }
                }
            }
            if (enterHighScore)
            {
                enterTextBox.Update();
            }
        }

        public static void Draw()
        {
            Gl.spriteBatch.Begin();
            if (state == MenuState.Main)
                DrawMain();
            else if (state == MenuState.DifficultySelect)
                DrawDifficultySelect();
            else if (state == MenuState.InGame)
                DrawInGame();
            else if (state == MenuState.HighScore)
                DrawHighScore();
            Gl.spriteBatch.End();
        }
        private static void DrawMain()
        {
            for (int i = 0; i < mainButtons.Count; i++)
            {
                mainButtons[i].Draw();
            }
        }
        private static void DrawDifficultySelect()
        {
            for (int i = 0; i < dffButtons.Count; i++)
            {
                dffButtons[i].Draw();
            }
        }
        private static void DrawInGame()
        {
            Gl.spriteBatch.Draw(Gl.dot, menuRec, backColor);
            if (showScore)
            {
                Vector2 size = Gl.Load<SpriteFont>("BigFont").MeasureString("Score: " + score.ToString());
                Gl.spriteBatch.DrawString(Gl.Load<SpriteFont>("BigFont"), "Score: " + score.ToString(), new Vector2(menuRec.Center.X - size.X / 2, menuRec.Y + 5 + size.Y / 2), Color.White);
            }
            else
            {
                Vector2 size = Gl.Load<SpriteFont>("BigFont").MeasureString("Paused...");
                Gl.spriteBatch.DrawString(Gl.Load<SpriteFont>("BigFont"), "Paused...", new Vector2(menuRec.Center.X - size.X / 2, menuRec.Y + 5 + size.Y / 2), Color.Black);
            }
            for (int i = 0; i < inGameButtons.Count; i++)
            {
                inGameButtons[i].Draw();
            }
        }
        private static void DrawHighScore()
        {
            for (int i = 0; i < highscoreButtons.Count; i++)
            {
                highscoreButtons[i].Draw();
            }
            for (int i = 0; i < 4; i++)
            {
                if (i == 0)
                {
                    Gl.DrawTextCentered(Gl.Load<SpriteFont>("BigFont"), new Vector2(Gl.graphics.Viewport.Width / 2, 200), "Easy Highscore", Color.White);
                    if (!(enterHighScore && enterDiffucultyLevel == Difficulty.Easy))
                        Gl.DrawTextCentered(highscoreFont, new Vector2(Gl.graphics.Viewport.Width / 2, 230), HighScoreManager.EasyHighScore.ToString() + " by " + HighScoreManager.EasyName, Color.White);
                    else
                        Gl.DrawTextFromRightBottom(Gl.Load<SpriteFont>("BiggerFont"), new Vector2(Gl.graphics.Viewport.Width / 2 - 5, 255), enterScore.ToString() + " by ", Color.White);
                }
                else if (i == 1)
                {
                    Gl.DrawTextCentered(Gl.Load<SpriteFont>("BigFont"), new Vector2(Gl.graphics.Viewport.Width / 2, 270), "Moderate Highscore", Color.Yellow);
                    if (!(enterHighScore && enterDiffucultyLevel == Difficulty.Moderate))
                        Gl.DrawTextCentered(highscoreFont, new Vector2(Gl.graphics.Viewport.Width / 2, 300), HighScoreManager.ModerateHighScore.ToString() + " by " + HighScoreManager.ModerateName, Color.Yellow);
                    else
                        Gl.DrawTextFromRightBottom(Gl.Load<SpriteFont>("BiggerFont"), new Vector2(Gl.graphics.Viewport.Width / 2 - 5, 325), enterScore.ToString() + " by ", Color.Yellow);
                }
                else if (i == 2)
                {
                    Gl.DrawTextCentered(Gl.Load<SpriteFont>("BigFont"), new Vector2(Gl.graphics.Viewport.Width / 2, 340), "Hard Highscore", Color.Red);
                    if (!(enterHighScore && enterDiffucultyLevel == Difficulty.Hard))
                        Gl.DrawTextCentered(highscoreFont, new Vector2(Gl.graphics.Viewport.Width / 2, 370), HighScoreManager.HardHighScore.ToString() + " by " + HighScoreManager.HardName, Color.Red);
                    else
                        Gl.DrawTextFromRightBottom(Gl.Load<SpriteFont>("BiggerFont"), new Vector2(Gl.graphics.Viewport.Width / 2 - 5, 395), enterScore.ToString() + " by ", Color.Red);
                }
                else if (i == 3)
                {
                    Gl.DrawTextCentered(Gl.Load<SpriteFont>("BigFont2"), new Vector2(Gl.graphics.Viewport.Width / 2, 430), "INSANE HIGHSCORE", Color.Lerp(Color.OrangeRed, Color.Yellow, 0.4f));
                    if (!(enterHighScore && enterDiffucultyLevel == Difficulty.Insane))
                        Gl.DrawTextCentered(Gl.Load<SpriteFont>("BiggerFont"), new Vector2(Gl.graphics.Viewport.Width / 2, 460), HighScoreManager.InsaneHighScore.ToString() + " by " + HighScoreManager.InsaneName, Color.Lerp(Color.OrangeRed, Color.Yellow, 0.4f));
                    else
                        Gl.DrawTextFromRightBottom(Gl.Load<SpriteFont>("BiggerFont"), new Vector2(Gl.graphics.Viewport.Width / 2 - 5, 485), enterScore.ToString() + " by ", Color.Lerp(Color.OrangeRed, Color.Yellow, 0.4f));

                }
            }
            if (enterHighScore)
            {
                enterTextBox.Draw();
            }
        }
    }
}
