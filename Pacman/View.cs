﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace Snake
{
    class View
    {
        /// <summary>
        /// the position of the center of the camera
        /// </summary>
        public static Vector2 position = new Vector2(0, 0);
        /// <summary>
        /// the zoom of the camera (default 1)
        /// </summary>
        public static float zoom = 1f;
        /// <summary>
        /// the strength from which the camera moves from position to
        /// destPosition from 0 to 1 (default 1 no interpolation)
        /// </summary>
        public static float strength = 1.0f / 10f;
        /// <summary>
        /// The position the camera is heading to
        /// </summary>
        public static Vector2 gotoPosition = new Vector2(0, 0);

        public static float ClampLeft = float.MinValue;
        public static float ClampRight = float.MaxValue;
        public static float ClampTop = float.MinValue;
        public static float ClampBottom = float.MaxValue;

        public static Vector2 actualScreenSize
        {
            get
            {
                return (new Vector2(Gl.graphics.Viewport.Width, Gl.graphics.Viewport.Height));
            }
        }
        public static Vector2 actualScreenCenter
        {
            get
            {
                return (actualScreenSize / 2);
            }
        }
        public static Vector2 screenSize
        {
            get
            {
                return (new Vector2(actualScreenSize.X / zoom, actualScreenSize.Y / zoom));
            }
        }
        public static Vector2 screenCenter
        {
            get
            {
                return (screenSize / 2);
            }
        }

        public static float LeftSide
        {
            get
            {
                return (position - new Vector2(screenCenter.X, screenCenter.Y)).X;
            }
        }
        public static float RightSide
        {
            get
            {
                return (position + new Vector2(screenCenter.X, screenCenter.Y)).X;
            }
        }
        public static float BottomSide
        {
            get
            {
                return (position + new Vector2(screenCenter.X, screenCenter.Y)).Y;
            }
        }
        public static float TopSide
        {
            get
            {
                return (position - new Vector2(screenCenter.X, screenCenter.Y)).Y;
            }
        }

        public static float One
        {
            get
            {
                return 1.0f / zoom;
            }
        }

        public static void Initialize()
        {
            gotoPosition = actualScreenSize / 2;
            position = gotoPosition;

        }

        public static void Update()
        {
            if (position != gotoPosition)
            {
                position += (gotoPosition - position) * strength;
            }
            CheckClamps();
        }

        public static void BasicMovement(float speed, bool allowZoom, bool slowerWhenZoomed)
        {
            if (Input.KeyDown(Keys.W))
            {
                gotoPosition.Y -= (slowerWhenZoomed) ? speed / zoom : speed;
            }
            if (Input.KeyDown(Keys.A))
            {
                gotoPosition.X -= (slowerWhenZoomed) ? speed / zoom : speed;
            }
            if (Input.KeyDown(Keys.S))
            {
                gotoPosition.Y += (slowerWhenZoomed) ? speed / zoom : speed;
            }
            if (Input.KeyDown(Keys.D))
            {
                gotoPosition.X += (slowerWhenZoomed) ? speed / zoom : speed;
            }
            if (allowZoom)
            {
                View.zoom *= 1.0f + (0.1f * Input.MouseScroll);
            }
        }

        static void CheckClamps()
        {
            if (LeftSide < ClampLeft)
            {
                position.X = ClampLeft + screenCenter.X;
            }
            if (RightSide > ClampRight)
            {
                position.X = ClampRight - screenCenter.X;
            }
            if (BottomSide > ClampBottom)
            {
                position.Y = ClampBottom - screenCenter.Y;
            }
            if (TopSide < ClampTop)
            {
                position.Y = ClampTop + screenCenter.Y;
            }
            if (LeftSide <= ClampLeft && RightSide >= ClampRight)
            {
                position.X = ClampLeft + (ClampRight - ClampLeft) / 2;
            }
            if (TopSide <= ClampTop && BottomSide >= ClampBottom)
            {
                position.Y = ClampTop + (ClampBottom - ClampTop) / 2;
            }
        }

        public static Vector2 relPos(Vector2 worldPos)
        {
            return (worldPos - position + screenCenter) * zoom;
        }
        public static Rectangle relRec(Rectangle rec)
        {
            return (new Rectangle((int)((rec.X - position.X + screenCenter.X) * zoom), (int)((rec.Y - position.Y + screenCenter.Y) * zoom), (int)(rec.Width * zoom), (int)(rec.Height * zoom)));
        }

        public static Vector2 worldPos(Vector2 relPos)
        {
            return ((relPos - actualScreenCenter) / zoom) + position;
        }

        public static Vector2 MousePos
        {
            get
            {
                return worldPos(new Vector2(Gl.MousePos.X, Gl.MousePos.Y));
            }
        }

        public static void Draw(Texture2D texture, Vector2 position, Color color)
        {
            Draw(texture, position, null, color, 0f, Vector2.Zero, Vector2.One, SpriteEffects.None, 0f);
        }
        public static void Draw(Texture2D texture, Vector2 position, Rectangle? sourceRectangle, Color color)
        {
            Draw(texture, position, sourceRectangle, color, 0f, Vector2.Zero, Vector2.One, SpriteEffects.None, 0f);
        }
        public static void Draw(Texture2D texture, Vector2 position, Rectangle? sourceRectangle, Color color, float rotation)
        {
            Draw(texture, position, sourceRectangle, color, rotation, Vector2.Zero, Vector2.One, SpriteEffects.None, 0f);
        }
        public static void Draw(Texture2D texture, Vector2 position, Rectangle? sourceRectangle, Color color, float rotation, Vector2 origin)
        {
            Draw(texture, position, sourceRectangle, color, rotation, origin, Vector2.One, SpriteEffects.None, 0f);
        }
        public static void Draw(Texture2D texture, Vector2 position, Rectangle? sourceRectangle, Color color, float rotation, Vector2 origin, Vector2 scale)
        {
            Draw(texture, position, sourceRectangle, color, rotation, origin, scale, SpriteEffects.None, 0f);
        }
        public static void Draw(Texture2D texture, Vector2 position, Rectangle? sourceRectangle, Color color, float rotation, Vector2 origin, float scale, SpriteEffects effects, float layerDepth)
        {
            Draw(texture, position, sourceRectangle, color, rotation, origin, new Vector2(scale, scale), effects, layerDepth);
        }
        public static void Draw(Texture2D texture, Vector2 position, Rectangle? sourceRectangle, Color color, float rotation, Vector2 origin, Vector2 scale, SpriteEffects effects, float layerDepth)
        {
            Gl.spriteBatch.Draw(texture, relPos(position), sourceRectangle, color, rotation, origin, scale * zoom, SpriteEffects.None, layerDepth);
        }

        public static void Draw(Texture2D texture, Rectangle rectangle, Color color)
        {
            Draw(texture, rectangle, null, color, 0f, Vector2.Zero, SpriteEffects.None, 0f);
        }
        public static void Draw(Texture2D texture, Rectangle rectangle, Rectangle? sourceRectangle, Color color)
        {
            Draw(texture, rectangle, sourceRectangle, color, 0f, Vector2.Zero, SpriteEffects.None, 0f);
        }
        public static void Draw(Texture2D texture, Rectangle rectangle, Rectangle? sourceRectangle, Color color, float rotation)
        {
            Draw(texture, rectangle, sourceRectangle, color, rotation, Vector2.Zero, SpriteEffects.None, 0f);
        }
        public static void Draw(Texture2D texture, Rectangle rectangle, Rectangle? sourceRectangle, Color color, float rotation, Vector2 origin)
        {
            Draw(texture, rectangle, sourceRectangle, color, rotation, origin, SpriteEffects.None, 0f);
        }
        public static void Draw(Texture2D texture, Rectangle rectangle, Rectangle? sourceRectangle, Color color, float rotation, Vector2 origin, SpriteEffects effects)
        {
            Draw(texture, rectangle, sourceRectangle, color, rotation, origin, effects, 0f);
        }
        public static void Draw(Texture2D texture, Rectangle rectangle, Rectangle? sourceRectangle, Color color, float rotation, Vector2 origin, SpriteEffects effects, float layerDepth)
        {
            Gl.spriteBatch.Draw(texture, relRec(rectangle), sourceRectangle, color, rotation, origin, effects, layerDepth);
        }

        public static void DrawString(SpriteFont font, string text, Vector2 position, Color color)
        {
            DrawString(font, text, position, color, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
        }
        public static void DrawString(SpriteFont font, string text, Vector2 position, Color color, float rotation, Vector2 origin, float scale, SpriteEffects effects, float layerDepth)
        {
            Gl.spriteBatch.DrawString(font, text, relPos(position), color, rotation, origin, scale * zoom, effects, layerDepth);
        }
    }

    class View3d
    {
        public static Matrix viewMatrix;
        public static Matrix projMatrix;
        public static Matrix worldMatrix;

        public static Vector3 position;
        //public static Vector3 lookAt;

        public static float zRot = 0f;
        public static float xRot = 0f;

        public static void Initialize()
        {
            worldMatrix = Matrix.Identity;
        }

        public static void Update()
        {
            viewMatrix = Matrix.CreateLookAt(position, lookAt, new Vector3(0, 0, 1));
            projMatrix = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, Gl.graphics.Viewport.AspectRatio, 1.0f, 5000.0f);

        }

        //public static void Set(float xRotation, float zRotation)
        //{
        //    float xRot = MathHelper.ToRadians(xRotation);
        //    float zRot = MathHelper.ToRadians(zRotation);
        //    float distance = 30;
        //    float z = distance * (float)Math.Sin(zRot);
        //    distance = distance * (float)Math.Cos(zRot);
        //    float x = distance * (float)Math.Cos(xRot);
        //    float y = distance * (float)Math.Sin(xRot);
        //    lookAt = position + Calc.dirVec(xRotation, zRotation, 30, true);
        //}
        //public static float xDir
        //{
        //    get
        //    {
        //        return ((float)Math.Atan2(
        //            lookAt.Y - position.Y,
        //            lookAt.X - position.X));
        //    }
        //}
        //public static float zDir
        //{
        //    get
        //    {
        //        float distance = Vector2.Distance(new Vector2(lookAt.X, lookAt.Y), new Vector2(position.X, position.Y));
        //        return ((float)Math.Atan2(
        //            lookAt.Z,
        //            distance));
        //    }
        //}

        public static Vector3 lookAt
        {
            get
            {
                return (position + (30 * Calc.dirVec(xRot, zRot, true)));
            }
            set
            {
                float distance = Vector2.Distance(new Vector2(lookAt.X, lookAt.Y), new Vector2(position.X, position.Y));
                zRot = MathHelper.ToDegrees((float)Math.Atan2(value.Z - position.Z, distance));
                xRot = MathHelper.ToDegrees((float)Math.Atan2(value.Y - position.Y, value.X - position.X));
            }
        }
    }
}
